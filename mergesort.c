#include <stdio.h>
#include <stdlib.h>

void merge(int * a, int *aux, int l, int m, int r){
  int i,j,k;

  //Quando aux estiver ordenado faz uma c�pia no array original
  if(aux[m] <= aux[m+1]){
    for(k=l; k<=r; k++) a[k] = aux[k];
    return;
  }

  //Funde as duas listas
  for(i=m+1; i > l; i--) aux[i-1] = a[i-1];
  for(j=m; j < r; j++) aux[r+m-j] = a[j+1];

  //InsertionSort
  for(k=l; k<=r; k++){
    i = aux[k];
    j = k;
        while(i < aux[j-1] && j > 0){
            aux[j] = aux[j-1];
            j--;
        }
        aux[j] = i;
	}


    print_v(aux, 6);

}

void mergeSort(int *v, int *aux, int n){
    memcpy(aux, v, (sizeof(int) * n));
    mergeSort_ordena(v, aux, 0, n-1);
}

void mergeSort_ordena(int *v, int *aux, int esq, int dir){
    if(esq == dir) return;

    int meio = (esq + dir) / 2;

    int q = 1;
    for(int i=esq; i < dir; i++){
        if(v[i] > v[i+1]){
            q = 0;
        }
    }
    if(q)return;

    printf("\nda\n");
    mergeSort_ordena(v, aux, esq, meio);
    mergeSort_ordena(v, aux, meio+1, dir);
    if(v[meio] <= v[meio+1]) return;

    merge(v, aux, esq, meio, dir);
}

void print_v(long int * v, long int n){
	long int i;
	for (i = 0; i < n; ++i) {
		printf("%d ", v[i]);
	}
	printf("\n");
}

void main(){

    int t = 8;
    int ar[] = {1,7,5,3,2,4,2,3};
    int * aux = (int*) malloc(sizeof(int) * t);

    mergeSort(ar, aux, t);

    print_v(ar, t);

    free(aux);
}

